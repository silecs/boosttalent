<?php
/**
 * @package    theme_boosttalent
 * @copyright  2022 Silecs {@link http://www.silecs.info/societe}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2021051700;
$plugin->requires  = 2021051100;
$plugin->component = 'theme_boosttalent';

$plugin->dependencies = [
    'theme_boost' => 2021051700
];
