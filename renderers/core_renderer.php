<?php
/**
 * @package    theme
 * @subpackage boosttalent
 * @copyright  2022 Silecs {@link http://www.silecs.info/societe}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

class theme_boosttalent_core_renderer extends theme_boost\output\core_renderer {

    public function image_url($imagename, $component = 'moodle') {
        // Strip -24, -64, -256  etc from the end of filetype icons so we
        // only need to provide one SVG, see MDL-47082.
        $imagename = \preg_replace('/-\d\d\d?$/', '', $imagename);
        return $this->page->theme->image_url($imagename, $component);
    }

    /**
     * Ajoute le logo de la gégion en pieds de page
     */
    public function get_url_logo_talent() {
        $html = $this->image_url('logo_TALENT_couleurs', 'theme');
        return $html;
    }
}
